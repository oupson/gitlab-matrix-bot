package main

import (
	"fmt"
	"log"
)

func main() {
	fmt.Printf("hello\n")

	config, err := ReadConfig("config.yaml")
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("%+v\n", config)
}
