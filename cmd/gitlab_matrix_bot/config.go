package main

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	MatrixConfig    MatrixConfig              `yaml:"matrix-config"`
	GitlabInstances map[string]GitlabInstance `yaml:"gitlab-config"`
}

type MatrixConfig struct {
	HostServer string `yaml:"host-server"`
	Username   string `yaml:"username"`
	Password   string `yaml:"password"`
}

type GitlabInstance struct {
	Credentials GitlabCredentials `yaml:"credentials"`
}

type GitlabCredentials struct {
	PersonnalAccessToken string `yaml:"personnal-access-token"`
}

func ReadConfig(configPath string) (*Config, error) {
	content, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	var config Config
	err = yaml.Unmarshal(content, &config)

	if err != nil {
		return nil, err
	}

	return &config, nil
}
