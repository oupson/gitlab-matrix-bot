all: bin/gitlab_matrix_bot

bin:
	@mkdir bin

SRCS=$(wildcard internal/**/**/*.go)

bin/gitlab_matrix_bot: bin cmd/gitlab_matrix_bot/*.go $(SRCS)
	@mkdir -p bin
	@go build -o $@ cmd/gitlab_matrix_bot/*.go

.PHONY: test clean

clean:
	@rm -rf bin coverage

test:
	@mkdir -p coverage
	@echo "TODO"